# REST API для получения курса валют
## Задание
```
Напишите REST API для получения курса валют с одним методом
который используется для получения валюты
Метод Используется для получения курса валюты
Входной параметр массив $params;
	$params = [
		'currency' = Обязательный параметр, код валюты
		'rateCurrency' = Необязательный параметр, код валюты, в которой выведется курс, default RUR (например 1 USD = 70 RUR);
		'rateSum' = Необязательный параметр, Сумма первой валюты, default 1. Если sum = 2, то результат будет 10 USD = 700 RUR;
	];
Ответ в формате json 
{
	"name": "Доллар",
	"code": "USD",
	"result": "700",
	"rateCurrency": "RUR",
	"rateSum": "10", // количество долларов на размен
	"rate": "70"
}
В работе использовать фреймворк Yii2
Для получения валюты использовать curl
```
## Реализация
#### Использовал следующие репозитории:
<ol>
<li>[Swap](https://github.com/florianv/swap.git)</li>
<li>[yii2-cbrf-rates](https://github.com/microinginer/yii2-cbrf-rates.git)</li>
</ol>

```
php composer.phar install
```

#### cURL
```
curl --location --request POST 'http://api.exchange-rates.local/api/v1/rate/get-currency' \
--header 'Accept: application/json' \
--form 'params="{\"currency\":\"EUR\",\"rateCurrency\":\"RUR\",\"rateSum\":3}"'
```

<p align="center">
        <a href="https://ibb.co/2Pc40xM"><img src="https://i.ibb.co/GkCSfyM/2021-05-17-01-40-30.png" alt="2021-05-17-01-40-30" border="0"></a>
    <br>
</p>

DIRECTORY STRUCTURE
-------------------

```
api
    config               contains shared configurations
    modules              contains api-specific model classes
    runtime              contains files generated during runtime
    web                  contains the entry script and Web resources
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
