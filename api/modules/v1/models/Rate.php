<?php

namespace api\modules\v1\models;

use yii\base\Model;
use Yii;
use Swap\Builder;

/**
 * Class Rate
 * @package api\modules\v1\models
 */
class Rate extends Model
{
    public $currency;
    public $rateCurrency;
    public $rateSum;

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['currency'], 'required'],
            [['currency', 'rateCurrency'], 'string'],
            [['rateSum'], 'integer'],
            ['rateCurrency', 'default', 'value' => 'RUR'],
            ['rateSum', 'default', 'value' => 1],
        ];
    }

    /**Get Currency
     * Gets the exchange rate
     *
     * @return array
     */
    public function getCurrency()
    {
        $swap = self::getSwap($this->currency, $this->rateCurrency);

        $name = self::getName($this->currency);

        return [
            'name' => $name,
            'code' => $swap->getCurrencyPair()->getBaseCurrency(),
            'result' => self::Calculator($swap->getValue(), $this->rateSum),
            'rateCurrency' => $swap->getCurrencyPair()->getQuoteCurrency(),
            'rateSum' => $this->rateSum,
            'rate' => $swap->getValue(),
        ];
    }

    /**Calculator
     * Calculation of the result
     *
     * @param $rate
     * @param $rateSum
     * @return float|int
     */
    public static function Calculator($rate, $rateSum)
    {
        return $rate * $rateSum;
    }

    /**Get Name
     * Obtaining the name of the currency in Russian
     *
     * @param $currency
     * @return mixed
     */
    public static function getName($currency)
    {
        $CbRF = Yii::$app->CbRF->filter(['currency' => $currency])->all();

        return $CbRF[$currency]['name'];
    }

    /** Swap
     * Getting currency rates
     *
     * @param $currency
     * @param $rateCurrency
     * @return \Exchanger\Contract\ExchangeRate
     */
    public static function getSwap($currency, $rateCurrency)
    {
        $swap = (new Builder())
            ->add('abstract_api', ['api_key' => '975132762cbd4f9d8343da64fc5b1f74'])
            ->add('european_central_bank')
            ->add('national_bank_of_romania')
            ->add('central_bank_of_republic_turkey')
            ->add('central_bank_of_czech_republic')
            ->add('russian_central_bank')
            ->add('bulgarian_national_bank')
            ->add('webservicex')
            ->build();

        if (strcasecmp($rateCurrency, 'RUR') === 0) {
            $rateCurrency = 'RUB';
        }

        if (strcasecmp($currency, 'RUR') === 0) {
            $rateCurrency = 'RUB';
        }

        return $swap->latest($currency . '/' . $rateCurrency);
    }
}