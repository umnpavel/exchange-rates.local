<?php

namespace api\modules\v1\controllers;


use Yii;
use yii\rest\Controller;
use api\modules\v1\models\Rate;
use yii\web\BadRequestHttpException;


/**
 * Class RateController
 * @package api\modules\v1\controllers
 */
class RateController extends Controller
{
    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'get-currency' => ['POST'],
        ];
    }

    /**Get Currency
     * Gets the exchange rate
     *
     * Request: POST /api/v1/rate/get-currency
     * @param $params
     * {"currency":"EUR","rateCurrency":"RUR","rateSum":3}
     *
     * @return array
     * {
     *  "name": "Евро",
     *  "code": "EUR",
     *  "result": 268.8648,
     *  "rateCurrency": "RUB",
     *  "rateSum": 3,
     *  "rate": 89.6216
     * }
     *
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetCurrency()
    {
        $rate = new Rate();

        $params = json_decode(\Yii::$app->getRequest()->getBodyParams()['params'], true);
        $rate->load($params, '');

        if (!$rate->validate()) {
            throw new BadRequestHttpException(
                'Invalid parameters: ' . json_encode($rate->getErrors())
            );
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $rate->getCurrency();
    }
}


